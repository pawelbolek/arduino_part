#include <Arduino.h>
#include <HX711.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>
#include <TimeLib.h>

#define HX711_DOUT_HEAD 5
#define HX711_SCLK_HEAD 4
#define HX711_DOUT_BOTTOM 12
#define HX711_SCLK_BOTTOM 13
#define DS_PIN 3

#define FIREBASE_HOST "nursinghomemanagment.firebaseio.com"
#define FIREBASE_AUTH "Wz5ChZ7mA6t08Fv2MWQBWyY5FwMMsKgNcfT77fgi"
#define WIFI_SSID "Hacknarok"
#define WIFI_PASSWORD "Hack2KPT"


int heartbeat=0;
double alpha = 0.75;
unsigned long previousMillis = 0;
double change = 0.00;

const byte channel_a=128;
const byte channel_b=32;
const byte channel_c=128;
const byte channel_d=32;
const int interval=200, strain_gauge_interval=1000, temp_interval=5000;

unsigned long previous_millis = 0, strain_gauge_millis=0, temp_millis=0;

float calibration_factor = -47000;
float calibration_factor_bottom = -47000;

HX711 head_strain_gauge(HX711_DOUT_HEAD,HX711_SCLK_HEAD);
HX711 bottom_strain_gauge(HX711_DOUT_BOTTOM,HX711_SCLK_BOTTOM);

OneWire ds_one_wire(DS_PIN);
DallasTemperature thermo(&ds_one_wire);
double temperature = 0.00;

void read_from_strain_gauge(HX711 strain_gauge, byte gain, String channel_name, int difference){
    strain_gauge.set_gain(gain);
    // head_strain_gauge.set_scale(calibration_factor);
    // head_strain_gauge.tare();
    Serial.print(channel_name+": ");
    char weight = strain_gauge.get_units(1)-difference;
    Serial.print(weight, 1);  //Up to 3 decimal points
    Serial.println(" kg");
    Firebase.setInt("weight", weight);
    Firebase.setString("channel_name", channel_name);
  }


String getCurrTime(){
  time_t t = now(); 

  //Serial.println("time status:");
  //Serial.println(timeStatus());
  //Serial.println("-----");
  
  String h = (String)hour();
  String mi = (String)minute();
  String sec = (String)second();
  //Serial.println("MILIS:");
//Serial.println(millis());
 // Serial.println(".");
  return h + ":" + mi + ":" + sec;
}




void setup(){                 
 // pinMode (led_Pin, OUTPUT);
  //Serial.begin(9600);
  
  pinMode(0,OUTPUT);
  Serial.begin(115200);

  thermo.begin();
  thermo.setResolution(9);
  delay(100);
  head_strain_gauge.set_scale(calibration_factor);
  head_strain_gauge.tare();
  delay(100);
  bottom_strain_gauge.set_scale(calibration_factor_bottom);
  bottom_strain_gauge.tare();

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());
  
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
}

void loop (){
    // initializing other variables
   static double oldValue = 0;
       static double oldChange = 0;
       unsigned long current_millis = millis();

    unsigned long currentMillis = millis();

    if (currentMillis - previousMillis >= interval) {
       
       previousMillis = currentMillis;
       
    

        float rawValue = analogRead(A0);                                         // Reading the sensors values
        double value = alpha*oldValue+(1-alpha)*rawValue; 

        Firebase.pushFloat("change_of_value", value);
        
        Firebase.setFloat("old_value", oldValue);
        Firebase.setFloat("raw_value", rawValue);
        Firebase.setFloat("last_change_of_value", value);

        change=value-oldValue;

      if(value>258){
          digitalWrite(0,(change>0.00&&oldChange<0.00));
      }
        oldValue = value;
        oldChange = change;
        Serial.print (rawValue);              // printing the sensor output value on the screen
        Serial.print (",");                                                                                          
        Serial.println (value);             // printing the heart beat value on the screen
        
  }

  if(current_millis - temp_millis >= temp_interval){
    temp_millis=current_millis;
    thermo.requestTemperatures();
    temperature = thermo.getTempCByIndex(0);
    Serial.println("Temperature: "+(String)temperature+" C");
    Firebase.setFloat("temperature", temperature);
  }

  if(current_millis - strain_gauge_millis >= strain_gauge_interval){
    strain_gauge_millis=current_millis;
    read_from_strain_gauge(head_strain_gauge,channel_a, "Channel A", 0);
    read_from_strain_gauge(head_strain_gauge,channel_b, "Channel B", 0);
    read_from_strain_gauge(bottom_strain_gauge,channel_c, "Channel C",0);
  //  read_from_strain_gauge(bottom_strain_gauge,channel_d, "Channel D");
  }
  
} 

